# Dollar

The project uses **[$1 Unistroke Recognizer](http://depts.washington.edu/madlab/proj/dollar/index.html)** as a base to provide gesture recognition over a first person character's camera.

It may potentially be used to recognize **mouse input and movement**.

#### Goal

To use this algorithm and try to record player’s mouse inputs in the game associating with different categories of movement.

## Available Technology

#### `Tracking Mode`

> To try recognizing what the stroke pattern’s representing state / category

#### `Record Mode`

> To cache the stroke traced to an array as a sample

## Potential Problems

1. Having issues with **insufficient reference points** on strokes
> - **No differentiation between points** will cause it to fail
> - In the ideal scenario, it should **throw an error** (unable to identify) when the trace is not enough in length
>   - The behaviour can be used for **tracking “Idling”**

2. Algorithm always try to give an answer, because it has no score threshold limit to test the results’ reliability
> - If there is only **one sample** in the templates, **that will be the answer, always**.
> - Not accurate to identify **unrecorded pattern**
>   - If an unrecognizable pattern is found, the AI lacks the ability to record it and name it accordingly (with association with the given categorisation)

3. Potentially **unstable memory leak**, system may fail if errors during the recognition process are not carefully asserted
> - On one testing occurance, the PC is forced to shut down due to Gigabytes of memory leak to `C:\`
>   - After rebooting the PC, it seems to become normal again

4. Player location tracking is world-based, which means **moving forward towards different directions doesn’t count the same**

5. AI would **not accept small values** like normalized axis values as the changes into them are very small
> - **Tweaks** on scale size and/or other variables are required

6. Lack of method to **import/export the samples**
> - Ideally, it must be able to load samples from file / export the data out from the game engine (**via JSON / CSV**)

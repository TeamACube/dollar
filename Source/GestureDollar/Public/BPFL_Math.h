// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "BPFL_Math.generated.h"

/**
 * 
 */
UCLASS()
class GESTUREDOLLAR_API UBPFL_Math : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

		UFUNCTION(BlueprintPure, Category="Math|Float")
		static float GetInfinity();
	
};
